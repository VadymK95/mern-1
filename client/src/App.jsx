import React, { useEffect } from 'react';
import Navbar from './components/navbar/Navbar';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import './app.css';
import Registration from './components/authorization/Registration';
import Login from './components/authorization/Login';
import { useDispatch, useSelector } from 'react-redux';
import { auth } from './actions/user';
import Disk from './components/disk/Disk';

const App = () => {
  const isAuth = useSelector(state => state.user.isAuth);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(auth());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <BrowserRouter>
      <div className="app">
        <Navbar />
        <div className="wrap">
          {!isAuth ? (
            <Switch>
              <Route path="/registration" component={Registration} />
              <Route path="/login" component={Login} />
              <Redirect to="/login" />
            </Switch>
          ) : (
            <Switch>
              <Route exact path="/" component={Disk} />
              <Redirect to="" />
            </Switch>
          )}
        </div>
      </div>
    </BrowserRouter>
  );
};

export default App;
