import React, { useState } from 'react';
import { registration } from '../../actions/user';
import Input from '../../utils/input/Input';
import './authorization.css';

const Registration = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <div className="authorization">
      <div className="authorization__header">Регистрация</div>
      <Input
        value={email}
        setValue={setEmail}
        type="text"
        placeholder="Введите email..."
      />
      <Input
        value={password}
        setValue={setPassword}
        type="text"
        placeholder="Введите password..."
      />
      <button
        onClick={() => registration(email, password)}
        className="authorization__button"
      >
        Зарегистрироваться
      </button>
    </div>
  );
};

export default Registration;
