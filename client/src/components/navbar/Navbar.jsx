import React from 'react';
import Logo from '../../assets/images/logo.png';
import { NavLink } from 'react-router-dom';
import './navbar.css';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../../reducers/userReducer';

const Navbar = () => {
  const isAuth = useSelector(state => state.user.isAuth);
  const dispatch = useDispatch();

  return (
    <div className="navbar">
      <div className="container">
        <img src={Logo} alt="logo" className="navbar__logo" />
        <div className="navbar__header">MERN CLOUD</div>
        {!isAuth && (
          <div className=" navbar__login">
            <NavLink className="navbar__link" to="/login">Войти</NavLink>
          </div>
        )}
        {!isAuth && (
          <div className="navbar__registration">
            <NavLink className="navbar__link" to="/registration">Регистрация</NavLink>
          </div>
        )}
        {isAuth && (
          <div onClick={() => dispatch(logout())} className="navbar__login">
            Выход
          </div>
        )}
      </div>
    </div>
  );
};

export default Navbar;
