import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createDir } from '../../actions/file';
import { setPopupDisplay } from '../../reducers/fileReducer';
import Input from '../../utils/input/Input';

const Popup = () => {
  const [dirName, setDirName] = useState('');
  const popupDisplay = useSelector(state => state.file.popupDisplay);
  const currentDir = useSelector(state => state.file.currentDir);
  const dispatch = useDispatch();

  function createHandler() {
        dispatch(createDir(currentDir, dirName));
  }

  return (
    <div
      onClick={() => dispatch(setPopupDisplay('none'))}
      className="popup"
      style={{ display: popupDisplay }}
    >
      <div
        onClick={event => event.stopPropagation()}
        className="popup__content"
      >
        <div className="popup__header">
          <div className="popup__title">Создать новую папку</div>
          <button
            onClick={() => dispatch(setPopupDisplay('none'))}
            className="popup__close"
          >
            X
          </button>
        </div>
        <Input
          type="text"
          className="popup__input"
          placeholder="Введите название папки..."
          value={dirName}
          setValue={setDirName}
        />
        <button onClick={() => createHandler()} className="popup__create">Создать</button>
      </div>
    </div>
  );
};

export default Popup;
